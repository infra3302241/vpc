# VPC
###########################################################################
output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "ID of VPC"
}

output "vpc_arn" {
  value       = module.vpc.vpc_arn
  description = "ARN of VPC"
}

output "vpc_cidr_block" {
  value       = module.vpc.vpc_cidr_block
  description = "CIDR Block of VPC"
}

output "private_subnet_ids" {
  value       = module.vpc.private_subnet_ids
  description = "IDs of Private Subnets"
}

output "private_route_table_ids" {
  value       = module.vpc.private_route_table_ids
  description = "IDs of Private Route Tables"
}

output "public_subnet_ids" {
  value       = module.vpc.public_subnet_ids
  description = "IDs of Public Subnets"
}

output "public_route_table_ids" {
  value       = module.vpc.public_route_table_ids
  description = "IDs of Public Route Tables"
}